import { Component, OnInit } from '@angular/core';

import { PostServiceService } from '../post-service.service';

@Component({
  selector: 'app-personal-posts',
  templateUrl: './personal-posts.component.html',
  styleUrls: ['./personal-posts.component.css']
})
export class PersonalPostsComponent implements OnInit {

constructor(public mfes: PostServiceService) {   }

ngOnInit() {
  this.posts = this.mfes.getPosts();
}

}
