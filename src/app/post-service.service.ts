import { Injectable } from '@angular/core';
import { POSTS } from './posts';

@Injectable({
  providedIn: 'root'
})
export class PostServiceService {


  newPost : Object ;

  public setMockPosts(){
    if(localStorage.getItem('posts') == null){
      localStorage.setItem('posts', JSON.stringify(POSTS));
    }
    else{
      return console.log('jau yra');
    }
  }

  public getPosts(): posts[]{
    let allPosts = JSON.parse(localStorage.getItem('posts'));
    allPosts == null ? [] : allPosts.posts;
    return allPosts;
  }
  public removeItem(Id){
    let allPosts = JSON.parse(localStorage.getItem('posts'));
    allPosts = allPosts.filter(post => post.postId != Id);
    localStorage.setItem('posts', JSON.stringify(allPosts));
    $(document).ready(function(){
      $('#'+Id).fadeOut('slow'); 
    });
  }
  public addItem(author,title,post,postId){
    let allPosts = JSON.parse(localStorage.getItem('posts'));
    if(postId == 0){
      postId = allPosts.length+1;
    }
    else{
      allPosts = allPosts.filter(post => post.postId != postId);
    }
    let newPost = {author,title,post,postId};
    allPosts.push(newPost);
    localStorage.setItem('posts', JSON.stringify(allPosts));
    return console.log(newPost);
  }
  public getPost(Id){
    let allPosts = JSON.parse(localStorage.getItem('posts'));
    return allPosts.filter(post => post.postId == Id);
    //return allPosts == null ? [] : allPosts.posts;
  }


  constructor() {

  }
}
