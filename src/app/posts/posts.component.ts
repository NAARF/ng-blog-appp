import { Component, OnInit} from '@angular/core';
import { Pipe} from "@angular/core";
import { PostServiceService } from '../post-service.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit {

  constructor(public mfes: PostServiceService) {   }

  ngOnInit() {
    this.posts = this.mfes.getPosts();
  }

}
