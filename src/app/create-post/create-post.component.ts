import { Component, OnInit } from '@angular/core';
import { Pipe} from "@angular/core";
import { PostServiceService } from '../post-service.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
model: any = {};
records = {}
params = {}
data = {}

  constructor(private adds: PostServiceService, private router: Router, private route: ActivatedRoute, private postService: PostServiceService, private location: PostServiceService){
  }
  ngOnInit() {
    const prodId = this.route.snapshot.paramMap.get('id');
    if(prodId > 0){
        let posts = this.adds.getPost(prodId);
        const data = posts[0];
        $(document).ready(function(){
          $("input[name='title']").val(data.title);
          $("textarea[name='post']").val(data.post);
        });
        }
  }
random(){

}
  onSubmit() {
    const prodId = this.route.snapshot.paramMap.get('id')
    if(prodId > 0){
      this.records = this.adds.addItem('Admin',this.model.title,this.model.post,prodId);
    }
    else{
      this.records = this.adds.addItem('Admin',this.model.title,this.model.post,0);
    }
      alert("Your post succesfully saved.");
      this.router.navigate(['/myposts']);
    }
}
