import { Component } from '@angular/core';
import { Pipe} from "@angular/core";
import { PostServiceService } from './post-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'blogApp';

  constructor(private mfs: PostServiceService) { }

  ngOnInit() {
    this.mfs.setMockPosts();
  }
}
