import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { PersonalPostsComponent } from './personal-posts/personal-posts.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { HeaderComponent } from './header/header.component';
import { SortPipe } from './sort.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MyPostsPipe } from './my-posts.pipe';
import { PostServiceService } from './post-service.service';


@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    PersonalPostsComponent,
    CreatePostComponent,
    HeaderComponent,
    SortPipe,
    MyPostsPipe
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [PostServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
