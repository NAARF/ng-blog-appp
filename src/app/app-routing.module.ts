import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalPostsComponent }      from './personal-posts/personal-posts.component';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { CreatePostComponent } from './create-post/create-post.component';

const routes: Routes = [
  { path: 'myposts', component: PersonalPostsComponent }
  { path: '', component: PostsComponent }
  { path: 'create', component: CreatePostComponent }
  { path: 'create/:id', component: CreatePostComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
