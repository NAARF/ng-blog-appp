import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPosts'
})
export class MyPostsPipe implements PipeTransform {

  transform(posts: Flyer[]) {
    return posts.filter(post => post.author == 'Admin');
  }

}
